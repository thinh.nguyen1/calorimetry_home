"""
Praktikum Digitalisierung
Kalorimetrie - Küchentischversuch

@author: Thinh Nguyen


"""

from functions import m_json
from functions import m_pck

path_setup_newton = "/home/pi/calorimetry_home/datasheets/setup_newton.json"

path_setup_heat_capacity = "/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json"

m_pck.check_sensors()

metadata_setup_newton = m_json.get_metadata_from_setup(path_setup_newton)
# metadata_setup_heat_capacity = m_json.get_metadata_from_setup(path_setup_heat_capacity)

m_json.add_temperature_sensor_serials("/home/pi/calorimetry_home/datasheets", metadata_setup_newton)
# m_json.add_temperature_sensor_serials("/home/pi/calorimetry_home/datasheets", metadata_setup_heat_capacity)

m_json.archiv_json('/home/pi/calorimetry_home/datasheets', path_setup_newton, '/home/pi/calorimetry_home/archive/Newton_Experiment')
# m_json.archiv_json('/home/pi/calorimetry_home/datasheets', path_setup_heat_capacity, '/home/pi/calorimetry_home/archive/Heat_Capacity_Experiment')

data_setup_newton = m_pck.get_meas_data_calorimetry(metadata_setup_newton)
# data_setup_heat_capacity = m_pck.get_meas_data_calorimetry(metadata_setup_heat_capacity)

m_pck.logging_calorimetry(data_setup_newton, metadata_setup_newton, '/home/pi/calorimetry_home/archive/Newton_Experiment', '/home/pi/calorimetry_home/datasheets')
# m_pck.logging_calorimetry(data_setup_heat_cap, metadata_setup_heat_cap, '/home/pi/calorimetry_home/archive/Heat_Capacity_Experiment', '/home/pi/calorimetry_home/datasheets')

print("fertig")



